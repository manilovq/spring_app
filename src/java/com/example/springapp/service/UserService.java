package com.example.springapp.service;

import com.example.springapp.model.User;

/**
 * Service class for {@link com.example.springapp.model.User}
 * @author Pavel Ignatov
 * @version 1.0
 */
public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
